import { Fragment, useState, useEffect, useContext  } from 'react';
import DashboardCard from '../components/DashboardCard';
import AdminModal from '../components/AdminModal';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import UserContext from '../UserContext';

export default function Dashboard() {
  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then(response => response.json())
      .then(data => {
        setProducts(data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);


return user.isAdmin && (
  <Tabs defaultActiveKey="users" id="uncontrolled-tab-example" className="mb-3">
    <Tab eventKey="users" title="Users">
      <AdminModal />

    </Tab>
    <Tab eventKey="products" title="Products">
      <AdminModal />
      {products.map(product => (
        <DashboardCard key={product._id} productProp={product} />
      ))}
    </Tab>
  </Tabs>
);

     /* MAO NI SULOD SA LINE 27
     {users.map(user => (
        <DashboardCard key={user._id} userProp={user} />
      ))}*/


}
